# latextemplate

This is a script that sets up an environment to create LaTeX documents and
creates skeleton LaTeX files. The skeleton generated was customized to my needs
- the are lots of sample TeX code since I tend to copy-paste LaTeX from the
internet a lot. The intended usage is uncomment and adapt whatever is useful
for any given situation.

If someone other than me is actually reading this, I hope this script will be
useful to you.


## Installation

1. Clone and build LaTeX makefile from
   <https://code.google.com/p/latex-makefile/>.

2. Modify LATEX_MAKEFILE_PATH variable at the top of the `latextemplate` file
   to point to the Makefile built in the previous step.

3. Add `latextemplate` file to your `$PATH`.


## Usage

Run

    latextemplate --help

for up-to-date usage information.


## Reporting Bugs / Sending Feedback

If you have something you want me to know, contact me via Bitbucket or send me
an email at <gliktaras@gmail.com>.


## Licensing

This project uses the BSD 2-Clause license.
